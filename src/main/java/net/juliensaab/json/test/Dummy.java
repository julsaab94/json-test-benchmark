package net.juliensaab.json.test;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created at 10/19/2017.
 *
 * @author Julien Saab <julsaab94@gmail.com>
 */
public class Dummy implements Serializable {
    private String dummyTitle1, dummyTitle2, dummyTitle3;
    private int number1;
    private double number2;
    private List<String> listOfVeryImportantData;
    private Map<String, Integer> veryOptimizedIndexes;
    private Date date;

    public String getDummyTitle1() {
        return dummyTitle1;
    }

    public void setDummyTitle1(String dummyTitle1) {
        this.dummyTitle1 = dummyTitle1;
    }

    public String getDummyTitle2() {
        return dummyTitle2;
    }

    public void setDummyTitle2(String dummyTitle2) {
        this.dummyTitle2 = dummyTitle2;
    }

    public String getDummyTitle3() {
        return dummyTitle3;
    }

    public void setDummyTitle3(String dummyTitle3) {
        this.dummyTitle3 = dummyTitle3;
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public double getNumber2() {
        return number2;
    }

    public void setNumber2(double number2) {
        this.number2 = number2;
    }

    public List<String> getListOfVeryImportantData() {
        return listOfVeryImportantData;
    }

    public void setListOfVeryImportantData(List<String> listOfVeryImportantData) {
        this.listOfVeryImportantData = listOfVeryImportantData;
    }

    public Map<String, Integer> getVeryOptimizedIndexes() {
        return veryOptimizedIndexes;
    }

    public void setVeryOptimizedIndexes(Map<String, Integer> veryOptimizedIndexes) {
        this.veryOptimizedIndexes = veryOptimizedIndexes;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
