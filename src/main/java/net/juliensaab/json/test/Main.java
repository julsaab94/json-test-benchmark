package net.juliensaab.json.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.github.benas.randombeans.api.EnhancedRandom;

import java.io.*;
import java.util.List;

/**
 * Created at 10/19/2017.
 *
 * @author Julien Saab <julsaab94@gmail.com>
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Gson gson = new GsonBuilder().create();
        List<Dummy> dummies = EnhancedRandom.randomListOf(100000, Dummy.class);
        final long startTime = System.currentTimeMillis();
        try (Writer writer = new BufferedWriter(new FileWriter("dummies.json"))) {
            gson.toJson(dummies, writer);
            writer.flush();
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Converting data into JSON and writing to file took: " + (endTime - startTime) + " millis");
        dummies = null;

        final long startParseTime = System.currentTimeMillis();
        try (Reader reader = new BufferedReader(new FileReader("dummies.json"))) {
            dummies = gson.fromJson(reader, new TypeToken<List<Dummy>>() {
            }.getType());
        }
        final long endParseTime = System.currentTimeMillis();
        System.out.println("Reading and Parsing data from JSON file took: " + (endParseTime - startParseTime) + " millis");
    }
}
